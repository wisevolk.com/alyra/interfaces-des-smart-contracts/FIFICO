const path = require("path");
const networkId = process.env.npm_package_config_ganache_networkId;
const gasPrice = process.env.npm_package_config_ganache_gasPrice;
const gasLimit = process.env.npm_package_config_ganache_gasLimit;

module.exports = {
    // See <http://truffleframework.com/docs/advanced/configuration>
    // to customize your Truffle configuration!
    contracts_build_directory: path.join(__dirname, "client/src/contracts"),
    networks: {
        develop: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*"
        },
        ganache: {
            host: "127.0.0.1",     // Localhost (default: none)
            port: 8545,            // Standard Ethereum port (default: none)
            network_id: networkId,       // Any network (default: none)
            gasPrice: gasPrice,
            gasLimit: gasLimit
        }
    },
    plugins: [ "solidity-coverage" ],
    compilers: {
        solc: {
            version: "0.6.2",
            settings: {
                optimizer: {
                    enabled: true,
                    runs: 200
                },
                evmVersion: "byzantium"
            }
        }
    }
};
