const { BN, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const { expect } = require("chai");

const FiFIToken = artifacts.require("FiFiToken");

contract("FiFiToken", accounts => {
    const owner = accounts[0];
    const wallet = accounts[1];
    const recipient = accounts[2];
    const recipient2 = accounts[3];
    const name = "FiFi Token";
    const symbol = "FiFi";
    const decimals = new BN("18");
    const initialAmount = 60;
    const totalSupply = new BN("60");
    const totalTokensFromContract = new BN((initialAmount * 10 ** decimals.toNumber()).toString());
    const approvalValue = new BN("1000");

    // Caps
    const softCap = new BN("1000");
    const hardCap = softCap.add(new BN("1")); // Augmentation du totalSupply plus 10%

    // Contribs
    const minContrib = new BN("500");
    const maxContrib = totalTokensFromContract.mul(new BN("5")).div(new BN("100"));

    let fifiToken;

    beforeEach(async () => {
        fifiToken = await FiFIToken.new(name, symbol, decimals, totalSupply, { from: owner });
    });

    it('Construction Token et passage des paramètres', async () => {
        const expectedTokens = initialAmount * 10 ** decimals;
        expect(await fifiToken.name()).to.be.equal(name);
        expect(await fifiToken.symbol()).to.be.equal(symbol);
        expect(await fifiToken.decimal()).to.be.bignumber.equal(decimals);
        expect(await fifiToken.totalSupply()).to.be.bignumber.equal(new BN(expectedTokens.toString()));
    });

    it('Test de la balance', async () => {
        expect(await fifiToken.balanceOf(owner)).to.be.bignumber.equal(totalTokensFromContract);
    });
    //
    it('Transfer invalide', async () => {
        const transferValue = totalTokensFromContract.add(new BN("1"));
        await expectRevert.unspecified(fifiToken.transfer(recipient, transferValue), { from: owner });
    });

    it("Transfer valide", async () => {
        const transferValue = new BN("1000");
        let transfert = await fifiToken.transfer.call(recipient, transferValue, { from: owner });
        expect(transfert).to.be.true;

        // Vérification des balances après un transfert valide.
        const expectedOwnerBalance = (new BN(await fifiToken.balanceOf(owner))).sub(transferValue);
        transfert = await fifiToken.transfer(recipient, transferValue, { from: owner });
        expect(await fifiToken.balanceOf(recipient)).to.be.bignumber.equal(transferValue);
        expect(await fifiToken.balanceOf(owner)).to.be.bignumber.equal(expectedOwnerBalance);

        // Verification de l'event Transfer
        expectEvent(transfert, "Transfer", { from: owner, to: recipient, value: transferValue });
    });

    it("Approval invalide", async () => {
        const badApprovalValue = totalTokensFromContract.add(new BN("1"));
        await expectRevert.unspecified(fifiToken.approve(recipient, badApprovalValue, { from: owner }));
    });

    it("Approval return True", async () => {
        const approval = await fifiToken.approve.call(recipient, approvalValue, { from: owner });
        expect(approval).to.be.true;
    });

    it("Test Approval value et l'émission de l'Event", async () => {
        // Vérification du montant autorisé
        const approval = await fifiToken.approve(recipient, approvalValue, { from: owner });
        expect(await fifiToken.allowance(owner, recipient)).to.be.bignumber.equal(approvalValue);

        // Vérification de l'event Approval
        expectEvent(approval, "Approval", { owner, spender: recipient, value: approvalValue });
    });

    it("Test de transferForm avec une valeur valide et controle de l'event de retour", async () => {
        const valueToTransfer = new BN("100");
        await fifiToken.approve(recipient, approvalValue, { from: owner });
        expect(await fifiToken.transferFrom.call(owner, recipient2, valueToTransfer, { from: recipient })).to.be.true;
        expectEvent(await fifiToken.transferFrom(owner, recipient2, valueToTransfer, { from: recipient }), "Transfer", {
            from: owner,
            to: recipient2,
            value: valueToTransfer
        });
    });

    //TODO ne fonctionne pas sur des valeurs valides
    it("Test de transferForm invalide - Valeur à transferer supérieure à l'allowance", async () => {
        const valueToTransfer = new BN("1");
        await fifiToken.approve(recipient, approvalValue, { from: owner });
        await expectRevert.unspecified(fifiToken.transferFrom.call(owner, recipient2, valueToTransfer), { from: recipient });
    });

    // Test de Caps
    it("Test de passage des caps", async () => {
        await fifiToken.setSoftCap(softCap);
        await fifiToken.setHardCap(hardCap);
        // console.log(hardCap);
        // console.log(await fifiToken.getSoftCap());
        expect(await fifiToken.getSoftCap()).to.be.bignumber.equal(softCap);
        expect(await fifiToken.getHardCap()).to.be.bignumber.equal(hardCap);
    });

    it("Test de passage des contributions max et min", async () => {
        await fifiToken.setMinContrib(minContrib);
        await fifiToken.setMaxContrib(maxContrib);
        expect(await fifiToken.getMinContrib()).to.be.bignumber.equal(minContrib);
        expect(await fifiToken.getMaxContrib()).to.be.bignumber.equal(maxContrib);
    });
});
