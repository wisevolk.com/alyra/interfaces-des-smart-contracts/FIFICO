pragma solidity ^0.5.0;

import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/token/ERC20/ERC20Detailed.sol";
import "github.com/OpenZeppelin/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract MyToken is ERC20,ERC20Detailed {

    constructor () public ERC20Detailed("Cordonnier", "CORE", 18) {
    }

    function SetAmount(address adr, uint amount) public {
        _mint(adr, amount);
    }
}

contract MyCrowdsal {

    using SafeMath for uint256;

    address private administrateur; // L'admin qui à déployé le contract et va setter des paramétres
    MyToken private token; // Le Token vendu.
    address payable wallet; // Un portefeuille (Metamask) où les fonds seront récoltés.

    struct Phase {
        string name;
        uint256 startDate;
        uint256 endDate;
        uint8 rate;         // le taux de vente (100 pour 100% et 50 pour 50%)
        bool useWhiteList;
    }
    Phase[] phases;  // Les phases de la levée de fonds

    uint256 tokensAviableForPurchasse; // Tokens disponible à la vente
    uint256 baseRateTokenForOneEth;  // Nombre de token données par Ether recu

    struct People {
        string name;
        address _address;
    }
    mapping( address => People ) whiteListe;

    struct Client {
        address payable _address;
        uint256 value;  // Pour rembourser exactement ce que l'acheteur à investi, meme s'il a fait plusieur investissements
    }
    Client[] acheteursAretribuer;

    modifier isAdmin() {
        require(administrateur == msg.sender, "Seul l'administrateur pour réaliser cette action");
        _;
    }

    modifier peutAcheter() {
        string memory phaseEnCours = "Levée de fond terminée!";
        bool canBy = false;
        uint i;
        for(i = 0; i<phases.length; i++ ) {
            if(phases[i].startDate <= now && now <= phases[i].startDate) {
                phaseEnCours = phases[i].name;
                if(phases[i].useWhiteList) {
                    canBy = whiteListe[msg.sender]._address==msg.sender;
                    break;
                } else  {
                    canBy = true;
                    break;
                }
            }
        }
        require(canBy, phaseEnCours);
        _;
    }

    modifier AddressIsNotZero(address addr) {
        require(addr != address(0), "Attention l'adresse n'est pas définie!");
        _;
    }

    modifier valueIsNotZero() {
        require(msg.value > 0,"La valeur ne doit pas etre zero.");
        _;
    }

    constructor(address payable pwallet) public AddressIsNotZero(pwallet) {
        require(pwallet != address(0), "Attention l'adresse reçevant les fonds n'est pas définie!");

        administrateur = msg.sender;
        wallet = pwallet;
        token = new MyToken();
        // see https://www.epochconverter.com/ pour les dates
        SetPhase("Vente privé", 1596240000, 1598832000, 50, true); // du 1.8.20 au 31.8.20
        SetPhase("Vente public", 1598918400, 1608508800, 100, false); // du 1.9.20 au 31.12.20

        tokensAviableForPurchasse = 10000000 * (10 ** uint256(18)); // Pas de using SafeMath ici car il faut que "ça" marche !
        // Transférer l'argent au token qu'il puisse la redistribuer
        token.SetAmount(address(this), tokensAviableForPurchasse);

        baseRateTokenForOneEth = 250000; // 250 K token par Eth

    }
    /**
    * Creation des phases, pourrait être public pour ne pas avoir des dates réglées en
    */
    function SetPhase(string memory name, uint256 startDate, uint256 endDate, uint8 rate, bool useWhiteList ) private {
        require(rate > 0, "Le taux ne doit pas etre ");
        Phase memory phase = Phase(name,startDate,endDate,rate, useWhiteList);
        phases.push(phase);
    }
    /**
    * Authoriser une personne à la phase de vente privée
    */
    function AddToWhiteList(string memory name, address paddress) public isAdmin AddressIsNotZero(paddress) {
        whiteListe[paddress] = People(name,paddress);
    }
    /**
    * Achat de token
     */
    function AcheterToken() public peutAcheter valueIsNotZero payable {
        // Calcul du nombre de tokens attribués
        uint256 temp = baseRateTokenForOneEth.mul(msg.value);
        temp = temp.mul(TauxDeLaPhaseEncours());
        uint256 nombreDeToken = temp.div(100);
        // Transferer les tokens à l'acheteur avec controle qu'il y ait assez de token disponibles
        token.transferFrom(address(this),msg.sender,nombreDeToken);
        // Si on est ici on peut récupérer l'argent
        wallet.transfer(msg.value);
        acheteursAretribuer.push(Client(msg.sender,msg.value));
    }
    /**
    * Montant disponible
    */
    function TotalSupply() public view returns (uint256) {
        return token.totalSupply();
    }
    /**
    * Get Taux De La Phase Encours
    */
    function TauxDeLaPhaseEncours() private view returns (uint256){
        uint i;
        for(i = 0; i<phases.length; i++) {
            if(phases[i].startDate<=now && now<=phases[i].startDate) {
                return phases[i].rate;
            }
        }
        revert("Levée de fond terminée!");
    }
    /**
    * Controler Levee De Fond Terminee
    */
    function ControlerLeveeDeFondTerminee() public {
        // As-t-on dépassé la date de fin
        bool leveeDeFondTerminee = true;
        uint i;
        for(i = 0; i<phases.length; i++) {
            if(phases[i].startDate<=now && now<=phases[i].startDate) {
                leveeDeFondTerminee = false;
                break;
            }
        }
        // La levee est terminée
        if(leveeDeFondTerminee) {
            if(TotalSupply() > 0) {
                // on a pas vendu assez
                Retribution();
            }
        }
    }
    /**
    * Rétribution en cas d’échec de la levée de fond de la participation de chaque donnateur
     */
    function Retribution() private {
        uint i;
        for(i = 0; i<acheteursAretribuer.length; i++) {
            acheteursAretribuer[i]._address.transfer(acheteursAretribuer[i].value);
        }
    }
}