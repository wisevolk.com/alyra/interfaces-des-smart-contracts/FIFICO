const FiFiToken = artifacts.require("FiFiToken");
const FiFiTokenCaps = artifacts.require("FiFiTokenCaps");
const FiFiTokenContribs = artifacts.require("FiFiTokenContribs");
const FiFiTokenSalesPeriods = artifacts.require("FiFiTokenSalesPeriods");
const FiFiCO = artifacts.require("FiFiCO");
const MultisigWallet = artifacts.require("MultisigWallet");

module.exports = function (deployer) {
    deployer.deploy(FiFiTokenCaps);
    deployer.deploy(FiFiTokenContribs);
    deployer.deploy(FiFiTokenSalesPeriods);
    deployer.deploy(FiFiToken, "FiFi Token", "FIFI", 18, 1000000)
        .then(() => {
            return deployer.deploy(FiFiCO, FiFiToken.address, FiFiToken.address, 500);
        })
        .then((fifico) => {
            return deployer.deploy(MultisigWallet, fifico.address, 5);
    });
};
