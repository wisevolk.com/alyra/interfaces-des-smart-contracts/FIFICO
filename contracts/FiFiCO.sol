pragma solidity >=0.6.0 < 0.6.3;

import "./FiFiToken.sol";
import "./SafeMath.sol";

/// @title L'ICO de Fifi.
/// @author Wisevolk
/// @notice Sivouplé toi achète token moi.
contract FiFiCO {

    using SafeMath for uint256;

    address payable private admin;
    address payable private wallet; // lui attribuer le contrat du MultiSig

    FiFiToken private token;

    uint256 private tokensSold;
    uint256 private rate;
    uint256 private tokenPrice;

    bool private resolutionVoted;

    address[] private clients;

    mapping(address => uint256) private balanceClients;

    /// @notice évènement envoyé lors del a vente d'un token
    event Sell(address _buyer, uint256 _amount);


    modifier isNotAddressZero(){
        require(msg.sender != address(0));
        _;
    }

    modifier isAdmin(){
        require(msg.sender == admin);
        _;
    }

    constructor(FiFiToken _token, address payable _wallet, uint256 _rate) public{
        admin = msg.sender;
        token = _token;
        rate = _rate;
    }

    function getWallet() public view isAdmin returns (address){
        return wallet;
    }

    function getToken() public view returns (FiFiToken) {
        return token;
    }

    function getTotalSupply() public view returns (uint256) {
        return token.totalSupply();
    }

    function getRate() public view returns (uint256) {
        return rate;
    }

    function getBalance() public view isAdmin returns (uint256){
        return address(this).balance;
    }

    function getTokenSold() public view returns (uint256) {
        return tokensSold;
    }

    //TODO tester le passage d'au minimum le rate;
    function buyTokens() public payable returns (bool) {
        require(msg.value > rate, "Toi donner sous sivouplé, sinon toi pas FiFiToken");

        uint256 tokensToBuy = _calculateTokensToBuy(msg.value);
        _postTransfer(msg.sender, tokensToBuy);

        wallet.transfer(msg.value);
        tokensSold = tokensSold.add(tokensToBuy);
        balanceClients[msg.sender] = msg.value;

        emit Sell(msg.sender, msg.value);
        return true;
    }

    function _calculateTokensToBuy(uint256 amount) private view returns (uint256){
        return  amount.mul(rate).div(tokenPrice);
    }

    function calculateTokensToBuy(uint256 amount) public view returns (uint256){
        return amount.mul(rate).div(tokenPrice);
    }

    function _postTransfer(address _from, uint _amount) private view returns (bool) {
        require(_amount <= token.totalSupply(), "amount");
        require((tokensSold.add(_amount)) <= token.totalSupply(), "coucou");
        return true;
    }

    function resolutionICO() public isAdmin {
        //TODO intégrer le contrat MultiSigWallet.sol
        admin.transfer(address(this).balance);
    }

    // Lier à l'intégrtion du multisig
    function refundICO() public isAdmin {
        require(resolutionVoted);
        //En attendant le multisig
        for (uint256 i; i < clients.length; i++) {
            address payable client = address(uint160(clients[i]));
            uint256 valeurETH = balanceClients[client];
            client.transfer(valeurETH);
            delete balanceClients[clients[i]];
        }
    }
}
