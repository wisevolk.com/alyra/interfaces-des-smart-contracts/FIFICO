pragma solidity >=0.6.0 <=0.6.3;

/// @title Le Token de FiFi
/// @author Wisevolk
/// @notice You can use this contract for only the most basic simulation
/// @dev All function calls are currently implemented without side effects... I hope !!

import "./IERC20.sol";
import "./SafeMath.sol";
import "./FiFiTokenCaps.sol";
import "./FiFiTokenContribs.sol";
import "./FiFiTokenSalesPeriods.sol";


contract FiFiToken is IERC20, FiFiTokenCaps, FiFiTokenContribs, FiFiTokenSalesPeriods {

    using SafeMath for uint256;

    event Transfer(address indexed from, address indexed to, uint256 value);
    event Approval(address indexed owner, address indexed spender, uint256 value);

    string private _name;
    string private _symbol;

    address private tokenOwner;

    uint8 private _decimals;
    uint256 private _totalSupply;

    mapping(address => uint256) private balances;
    mapping(address => mapping(address => uint256)) private allowances;

    constructor(string memory name, string memory symbol, uint8 decimals, uint256 _initialAmount) public {
        tokenOwner = msg.sender;
        _name = name;
        _symbol = symbol;
        _decimals = decimals;
        _totalSupply = _initialAmount * 10 ** uint256(decimals);
        balances[msg.sender] = _totalSupply;
    }

    function name() public view returns (string memory){
        return _name;
    }

    function symbol() public view returns (string memory){
        return _symbol;
    }

    function decimal() public view returns (uint8){
        return _decimals;
    }

    function totalSupply() override external view returns (uint256){
        return _totalSupply;
    }

    function balanceOf(address _account) override external view returns (uint256){
        return balances[_account];
    }

    function transfer(address _recipient, uint256 _amount) override external returns (bool){
        require(balances[msg.sender] >= _amount);

        balances[msg.sender] = balances[msg.sender].sub(_amount);
        balances[_recipient] = balances[_recipient].add(_amount);

        emit Transfer(msg.sender, _recipient, _amount);
        return true;
    }

    function allowance(address _owner, address _spender) override external view returns (uint256){
        return allowances[_owner][_spender];
    }

    function approve(address _spender, uint256 _amount) override external returns (bool){
        require(balances[msg.sender] >= _amount);
        allowances[msg.sender][_spender] = 0;
        allowances[msg.sender][_spender] = _amount;

        emit Approval(msg.sender, _spender, _amount);
        return true;
    }

    function transferFrom(address _sender, address _recipient, uint256 _amount) override external returns (bool){
        require(allowances[_sender][msg.sender] >= _amount);
        require(balances[_sender] >= _amount);

        balances[_sender] = balances[_sender].sub(_amount);
        balances[_recipient] = balances[_recipient].add(_amount);

        emit Transfer(_sender, _recipient, _amount);
        return true;
    }
}
