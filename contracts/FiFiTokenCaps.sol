pragma solidity >=0.6.0 < 0.6.3;

contract FiFiTokenCaps {

    //TODO ajouter les méthodes pour définir le parent et bloquer les accès à partir de cette adresse

    uint256 internal softCap;
    uint256 internal hardCap;
    bool internal capsEnable;
    address internal admin;

    //TODO modifier les visibilités à internal pour plus de sécurité et voir comment résoudre ce problème avec les tests.


    //TODO implémenter les tests
    function setCaps(uint256 _softCap, uint256 _hardCap) public{
        setSoftCap(_softCap);
        setHardCap(_hardCap);
    }

    function setSoftCap(uint256 _softCap) public {
        if (hardCap == 0) {
            hardCap = _softCap;
        }
        require(hardCap >= _softCap);
        softCap = _softCap;
    }

    function getSoftCap() public view returns (uint256) {
        return softCap;
    }

    function setHardCap(uint256 _hardCap) public {
        require(_hardCap >= softCap);
        hardCap = _hardCap;
    }

    function getHardCap() public view returns (uint256) {
        return hardCap;
    }

    //TODO Réfléchir au PDC
    //function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual returns (bool){}
}
