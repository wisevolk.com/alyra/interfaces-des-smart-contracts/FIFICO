## MyToken
* Éviter les imports de librairies
* function setAmount : Essayer de respecter les conventions d'écriture à savoir paramètres débutants par "_"
 
## MyCrowdsale
* Organiser les variables par type et genre, regrouper les structures.
    * variables
    * tableaux
        * tableau phases remplaçable par une énumération.
    * mappings
    * Structures
        * variable classées par type
    * bien definir le scope de toute les variables : ex tokensAviableForPurchasse (petite coquille orthographe au passage)
* struct People{}
    * utilisation du "_" pour _address hors convention
* struct Client{}
    * utilisation du "_" pour _address hors convention
* modifier peutAcheter()
    * Éviter l'utilisation d'une boucle pour optimisation.
    * sortir la logique dans une fonction privée
        * (Les commentaires suivants concernent le code du modifier)
        * le "uint i" peut être déclaré directement dans la condition de boucle 
        * hamoniser l'indentation
        * `if(phases[i].startDate <= now && now <= phases[i].startDate) //pas enDate plutot ? ou phases[i].startDate == now ?
        `
* Constructeur
    * utilisation de SetPhase avec des arguments en dur,utilisé plutot le passage de paramètre dans le constructeur ou rendre la fonction public et la bloquer pour l'admin comme dans les commentaires de la fonction ;).
* SetPhase()
    * renommer en "_setPhase"
* AddToWhiteList
    * Passer à external
    * renommer en "addToWhiteList"
* idem pour les autres...
* AcheterToken
    * Passer à external
    * Passer le mot clé payable avant l'utilisation des modifiers
    * utilisation du modifier valueIsNotZero superflu car son utilisation est unique, intégrer le require dans la fonction ou dans le modifier peutAcheter
    * passer le `walle.transfer(msg.value)` avant `token.transferFrom(address(this),msg.sender,nombreDeToken)` si jamais le transfer échou.
* TotalSupply
    * Passer à external
* TauxDeLaPhaseEncours
    * renommer en _tauxDeLaPhaseEncours
    * "uint i" dans la déclaration for
* ControlerLeveeDeFondTerminee
    * rendre external et ajouter un modifier isAdmin même si le code controle le booléen la boucle consomme des gas.
* Retribution
    * uint dans la condition de boucle

### General
* Nommer le fichier du nom du contrat
* Éviter le passage d'une langue à une autre
* Déplacer les commentaires au dessus des partie à commenter
* Commenter le contrat et ses fonctionnalité et inclure sa licence
* Respecter les conventions d'écriture solidity
* Employer des enum lorsque cela est possible et évite d'utiliser trop de boucles.
* Pour les require utiliser des variables pour les messages plutôt que des strings en dur
* Utiliser des events pour confirmer ou infirmer l'exécution des functions.
* Utiliser des retours pour tester l'exécution de certaine functions.
* Réduire la portée des fonctions "public" en les rendant external
* Eviter de tester la length dans une boucle surtout pour des tableaux à taille variable et même si on le fait tous :)))))
 
## J'espère ne pas être passé à côté de la grosse bourde ;) 
