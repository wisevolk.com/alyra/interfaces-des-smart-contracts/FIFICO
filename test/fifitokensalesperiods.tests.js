const { BN, expectRevert, expectEvent, time, constants } = require("@openzeppelin/test-helpers");
const { expect } = require("chai");
const moment = require("moment");

const FiFiTokenSalesPeriods = artifacts.require("FiFiTokenSalesPeriods");
contract("FiFiTokenSalesPeriods", accounts => {
    const client1 = accounts[1];
    const client2 = accounts[2];

    const currentDate = moment().add(2, "d");
    const startPrivateSales = new BN(currentDate.unix());
    const endPrivateSales = new BN((currentDate.add(1, "M").unix()));
    const startPreSales = new BN((currentDate.add(3, "M")).unix());
    const endPreSales = startPreSales.add(new BN((currentDate.add(5, "M").unix())));

    let fiFiTokenSalesPeriods;

    beforeEach(async () => {
        fiFiTokenSalesPeriods = await FiFiTokenSalesPeriods.new();
    });

    it("Setting private Sales with date before today ==> Revert", async () => {
        time.increase(time.duration.days(2));
        const nextMonth = new BN((currentDate.add(1, "M")).unix());
        await expectRevert.unspecified(fiFiTokenSalesPeriods.setPrivateSales(startPrivateSales, nextMonth));
    });

    it("Setting private Sales with end before start date", async () => {
        const newStartDate = new BN((currentDate.add(5, "d")).unix());
        const newEndDate = new BN((currentDate.subtract(3, "d")).unix());
        await expectRevert.unspecified(fiFiTokenSalesPeriods.setPrivateSales(newStartDate, newEndDate));
    });

    //TODO Voir avec Yosra le problème de time de la blockchain, block.timestamp ne retourne pas le temps de la blockchain mais un objet.
    it("Setting private sales - Valid", async () => {
        await time.advanceBlock();
        const latestBlock = await time.latest();
        const start = new BN((moment(latestBlock).add(1, "M")).unix());
        const end = new BN(start.add(new BN((moment(latestBlock).add(3, "M")).unix())));
        await fiFiTokenSalesPeriods.setPrivateSales(start, end);
        expect(await fiFiTokenSalesPeriods.getPrivateSalesStartDate()).to.be.bignumber.equal(start);
        expect(await fiFiTokenSalesPeriods.getPrivateSalesEndDate()).to.be.bignumber.equal(end);
    });

    it("Add a user to private list with ZERO address", async () => {
        await expectRevert.unspecified(fiFiTokenSalesPeriods.addToPrivateList(constants.ZERO_ADDRESS));
    });

    it("Add a user to private list with user already added", async () => {
        await fiFiTokenSalesPeriods.addToPrivateList(client1);
        await expectRevert.unspecified(fiFiTokenSalesPeriods.addToPrivateList(client1));
    });

    it("Add a user to private list", async () => {
        expect(await fiFiTokenSalesPeriods.getPrivateList.call(client1)).to.be.false;
        expect(await fiFiTokenSalesPeriods.getPrivateListTab.call()).to.be.an("array").that.is.empty;
        await fiFiTokenSalesPeriods.addToPrivateList(client1);
        expect(await fiFiTokenSalesPeriods.getPrivateList.call(client1)).to.be.true;
        expect(await fiFiTokenSalesPeriods.getPrivateListTab.call()).to.have.lengthOf(1);
        expectEvent(await fiFiTokenSalesPeriods.addToPrivateList(client2), "PrivateClientAdded", { client: client2 });
    });

    it("Remove a user from private list with ZERO address", async () => {
        await expectRevert.unspecified(fiFiTokenSalesPeriods.removeFromPrivateList(constants.ZERO_ADDRESS));
    });

    it("Remove a user from private list with user already removed", async () => {
        await fiFiTokenSalesPeriods.addToPrivateList(client1);
        expect(await fiFiTokenSalesPeriods.getPrivateListTab.call()).to.have.lengthOf(1);
        await fiFiTokenSalesPeriods.removeFromPrivateList(client1);
        await expectRevert.unspecified(fiFiTokenSalesPeriods.removeFromPrivateList(client1));
    });

    it("Remove a user from private list", async () => {
        await fiFiTokenSalesPeriods.addToPrivateList(client1);
        expect(await fiFiTokenSalesPeriods.getPrivateList.call(client1)).to.be.true;
        await fiFiTokenSalesPeriods.removeFromPrivateList(client1);
        expect(await fiFiTokenSalesPeriods.getPrivateList.call(client1)).to.be.false;
        //TODO demander si l'ajout d'un mappin à un tableau se fait par référence ou par valeur.
        // await fiFiTokenSalesPeriods.addToPrivateList(client2);
        // expectEvent(await fiFiTokenSalesPeriods.removeFromPrivateList(client2), "PrivateClientRemoved", { client: client2 });
    });


    it("Setting pre-Sales with date before today ==> Revert", async () => {
        time.increase(time.duration.days(2));
        const nextMonth = new BN((currentDate.add(1, "M")).unix());
        await expectRevert.unspecified(fiFiTokenSalesPeriods.setPreSales(startPrivateSales, nextMonth));
    });

    it("Setting pre-Sales with end before start date", async () => {
        const newStartDate = new BN((currentDate.add(5, "d")).unix());
        const newEndDate = new BN((currentDate.subtract(3, "d")).unix());
        await expectRevert.unspecified(fiFiTokenSalesPeriods.setPreSales(newStartDate, newEndDate));
    });

    it("Setting pre-sales - Valid", async () => {
        await time.advanceBlock();
        const latestBlock = await time.latest();
        const start = new BN((moment(latestBlock).add(1, "M")).unix());
        const end = new BN(start.add(new BN((moment(latestBlock).add(3, "M")).unix())));
        await fiFiTokenSalesPeriods.setPreSales(start, end);
        expect(await fiFiTokenSalesPeriods.getPreSalesStartDate()).to.be.bignumber.equal(start);
        expect(await fiFiTokenSalesPeriods.getPreSalesEndDate()).to.be.bignumber.equal(end);

        // console.log(await time.latest());
    });

    it("Add a user to pre-list with ZERO address", async () => {
        await expectRevert.unspecified(fiFiTokenSalesPeriods.addToPreSaleList(constants.ZERO_ADDRESS));
    });

    it("Add a user to pre-list with user already added", async () => {
        await fiFiTokenSalesPeriods.addToPreSaleList(client1);
        await expectRevert.unspecified(fiFiTokenSalesPeriods.addToPreSaleList(client1));
    });

    it("Add a user to pre-list", async () => {
        expect(await fiFiTokenSalesPeriods.getPreList.call(client1)).to.be.false;
        expect(await fiFiTokenSalesPeriods.getPreListTab.call()).to.be.an("array").that.is.empty;
        await fiFiTokenSalesPeriods.addToPreSaleList(client1);
        expect(await fiFiTokenSalesPeriods.getPreList.call(client1)).to.be.true;
        expect(await fiFiTokenSalesPeriods.getPreListTab.call()).to.have.lengthOf(1);
        expectEvent(await fiFiTokenSalesPeriods.addToPreSaleList(client2), "PreClientAdded", { client: client2 });
    });

    it("Remove a user from pre-list with ZERO address", async () => {
        await expectRevert.unspecified(fiFiTokenSalesPeriods.removeFromPreSaleList(constants.ZERO_ADDRESS));
    });

    it("Remove a user from pre-list with user already removed", async () => {
        await fiFiTokenSalesPeriods.addToPreSaleList(client1);
        expect(await fiFiTokenSalesPeriods.getPreListTab.call()).to.have.lengthOf(1);
        await fiFiTokenSalesPeriods.removeFromPreSaleList(client1);
        await expectRevert.unspecified(fiFiTokenSalesPeriods.removeFromPreSaleList(client1));
    });

    it("Remove a user from pre-list", async () => {
        await fiFiTokenSalesPeriods.addToPreSaleList(client1);
        expect(await fiFiTokenSalesPeriods.getPreList.call(client1)).to.be.true;
        await fiFiTokenSalesPeriods.removeFromPreSaleList(client1);
        expect(await fiFiTokenSalesPeriods.getPreList.call(client1)).to.be.false;
        //TODO demander si l'ajout d'un mappin à un tableau se fait par référence ou par valeur.
        // await fiFiTokenSalesPeriods.addToPrivateList(client2);
        // expectEvent(await fiFiTokenSalesPeriods.removeFromPreSaleList(client2), "PrivateClientRemoved", { client: client2 });
    });
});
