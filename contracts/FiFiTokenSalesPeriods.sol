pragma solidity >=0.6.0 < 0.6.3;

contract FiFiTokenSalesPeriods {

    //TODO ajouter les méthodes pour définir le parent et bloquer les accès à partir de cette adresse

    event PrivateClientAdded(address client);
    event PrivateClientRemoved(address client);

    event PreClientAdded(address client);
    event PreClientRemoved(address client);

    uint256 private privateSalesStartDate;
    uint256 private privateSalesEndDate;
    uint256 private preSalesStartDate;
    uint256 private preSalesEndDate;

    bool internal periodsEnabled;
    address private child;

    mapping(address => bool) private privateList;
    mapping(address => bool) private preList;

    address[] private privateListTab;
    address[] private preListTab;

    modifier not0address(address _client) {
        require(_client != address(0));
        _;
    }

    //Private List
    function addToPrivateList(address _client) public not0address(_client) {
        require(privateList[_client] == false);
        privateList[_client] = true;
        privateListTab.push(_client);
        emit PrivateClientAdded(_client);
    }

    function removeFromPrivateList(address _client) public not0address(_client) {
        require(privateList[_client]);
        privateList[_client] = false;
        emit PrivateClientRemoved(_client);
    }

    function getPrivateList(address _client) public not0address(_client) returns (bool) {
        return privateList[_client];
    }

    function getPrivateListTab() public returns (address[] memory) {
        return privateListTab;
    }

    // Pre-List
    function addToPreSaleList(address _client) public not0address(_client) {
        require(preList[_client] == false);
        preList[_client] = true;
        preListTab.push(_client);
        emit PreClientAdded(_client);
    }

    function removeFromPreSaleList(address _client) public not0address(_client) {
        require(preList[_client]);
        preList[_client] = false;
        emit PreClientRemoved(_client);
    }

    function getPreList(address _client) public not0address(_client) returns (bool) {
        return preList[_client];
    }

    function getPreListTab() public returns (address[] memory) {
        return preListTab;
    }


    //Dates
    function setPrivateSales(uint256 _start, uint _end) public {
        require(_start > block.timestamp);
        require(_end > _start);
        privateSalesStartDate = _start;
        privateSalesEndDate = _end;
    }

    function getPrivateSalesStartDate() public view returns (uint256) {
        return privateSalesStartDate;
    }

    function getPrivateSalesEndDate() public view returns (uint256) {
        return privateSalesEndDate;
    }

    function setPreSales(uint256 _start, uint _end) public {
        require(_start >= block.timestamp);
        require(_end > _start);
        preSalesStartDate = _start;
        preSalesEndDate = _end;
    }

    function getPreSalesStartDate() public view returns (uint256) {
        return preSalesStartDate;
    }

    function getPreSalesEndDate() public view returns (uint256) {
        return preSalesEndDate;
    }
}
