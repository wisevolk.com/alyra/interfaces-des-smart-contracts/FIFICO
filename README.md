##TODOs :
* Intégrer le contrat MultisigWallet.
    * Créer les tests pour le Multisig.   
* Modifier le sens d'héritage du TokenFiFi ou faire appel aux Libraries plutot qu'à l'héritage.
* intégrer les commentaires Natspec.
* Ajouter les tests à FiFiToken

##Yosra :
* Pourquoi le block.timestamp ne renvoie pas un timestamp ?
* Précision sur les BN lors des tests (montant supérieur lors de buyTokens)

