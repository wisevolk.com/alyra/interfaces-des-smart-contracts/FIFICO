pragma solidity >=0.6.0 < 0.6.3;

import "./SafeMath.sol";

/// @title Contrat multisig pour l'acceptation de la résolution d'une Crowdsale.
/// @author Wisevolk
/// @notice À intégrer dans la résolution de FiFiCO
contract MultisigWallet {

    using SafeMath for uint256;

    /// @param numberOfSignatoriesAuthorized : nombre de signataires pour la résolution
    uint256 numberOfSignatoriesAuthorized;
    /// @param dureeBlocagefonds : durée de blocage des fonds avant de pouvoir les débloquer
    uint256 dureeBlocagefonds;
    /// @param resolutionApproved : true si la résolution est acceptée par tous les signataires.
    bool resolutionApproved;

    address contractOwner;
    mapping(address => uint256) private signatories;
    mapping(address => bool) private signatures;

    /// @notice Vérifie que le sender est bien le créateur du Multisig
    modifier isAdmin() {
        require(msg.sender == contractOwner);
        _;
    }

    /// @notice Vérifie que l'adresse fournie en paramètre n'est pas nulle en appelant la méthode privée _isNotAddressZero(address _address)
    /// @param _address : adresse du requérant
    modifier isNotAddressZero(address _address){
        require(_isNotAddressZero(_address));
        _;
    }

    /// @param _owner : adresse du contrat créateur du multisig
    /// @param _numberOfSignatoriesAuthorized : nombre des signataires autorisés
    constructor(address _owner, uint256 _numberOfSignatoriesAuthorized) public{
        require(_numberOfSignatoriesAuthorized > 0);
        contractOwner = _owner;
        numberOfSignatoriesAuthorized = _numberOfSignatoriesAuthorized;
    }

    /// @notice ajout d'un signataire si le nombre maximum n'est pas atteint
    /// @param _signatory : signataire à ajouter auquel on applique un poids de 1 par défaut
    function addSignatory(address _signatory) public isAdmin isNotAddressZero(_signatory) {
        require(numberOfSignatoriesAuthorized < 3);
        signatories[_signatory] = 1;
    }

    /// @notice augmentation du poids du vote par le poids fourni
    /// @param _signatory : signataire valide
    /// @param _weight : poids du signataire
    function raiseSignatureWeight(address _signatory, uint256 _weight) public isAdmin isNotAddressZero(_signatory) {
        require(signatories[_signatory] > 0);
        signatories[_signatory] = signatories[_signatory].add(_weight);
    }

    /// @notice signe la résolution du contrat
    /// @param _signatory : signataire du contrat
    function signContractResolution(address _signatory) public isNotAddressZero(_signatory) {
        require(signatories[msg.sender] > 0);
        signatures[_signatory] = true;
    }

    /// @return true si la résolution du contrat est approuvé
    function isResolutionApproved() public view isAdmin returns (bool) {
        return resolutionApproved;
    }

    /// @notice vérifie que l'adresse passée est différente de nulle
    /// @param _address : adresse à vérifier
    function _isNotAddressZero(address _address) private view returns (bool) {
        if (_address != address(0) && _address != address(this)) {
            return true;
        }
        return false;
    }
}
