pragma solidity >=0.6.0 < 0.6.3;

contract FiFiTokenContribs {

    //TODO ajouter les méthodes pour définir le parent et bloquer les accès à partir de cette adresse

    uint256 private minContrib;
    uint256 private maxContrib;

    bool internal contribEnabled;
    address private child;

    function setMinContrib(uint256 _minContrib) public  {
        if (maxContrib == 0) {
            maxContrib = _minContrib;
        }
        require(_minContrib <= maxContrib);
        minContrib = _minContrib;
    }

    function getMinContrib() public view returns (uint256) {
        return minContrib;
    }

    function setMaxContrib(uint256 _maxContrib) public {
        require(_maxContrib > minContrib);
        maxContrib = _maxContrib;
    }

    function getMaxContrib() public view returns (uint256) {
        return maxContrib;
    }

    //TODO Réfléchir au PDC
    //function _beforeTokenTransfer(address from, address to, uint256 amount) internal virtual returns (bool){}
}
