const { BN, expectEvent, expectRevert, constants, balance, ether } = require("@openzeppelin/test-helpers");
const { expect, should, assert } = require("chai");

const FiFiCO = artifacts.require("FiFiCO");
const FiFiToken = artifacts.require("FiFiToken");

contract("FiFiCO", accounts => {
    const tokenName = "FiFi Token";
    const symbol = "FiFi";
    const decimals = new BN(18);
    const initialAmount = 60;
    const totalSupply = new BN("60");
    const totalTokensFromContract = new BN((initialAmount * 10 ** decimals.toNumber()).toString());

    const owner = accounts[0];
    const buyer1 = accounts[1];
    const wallet = accounts[2];
    const rate = new BN("500000");

    let token;
    let fifiCO;

    beforeEach(async () => {
        token = await FiFiToken.new(tokenName, symbol, decimals, totalSupply, { from: owner });
        fifiCO = await FiFiCO.new(token.address, wallet, rate, { from: owner });
    });

    it("Construction du contrat de l'ICO", async () => {
        expect(await fifiCO.getToken()).to.be.equal(token.address);
        expect(await fifiCO.getRate()).to.be.bignumber.equal(rate);
    });

    it("Achat avec un montant inférieur au rate", async () => {
        const valueInfRate = new BN("250");
        await expectRevert.unspecified(fifiCO.buyTokens({ from: buyer1, value: valueInfRate }));
    });


    //TODO reprendre ici
    it("Achat avec un montant supérieur au totalSupply", async () => {

        const valueSupTotalSupply = totalTokensFromContract.add(new BN("1"));

        console.log("ValuetoSupply : " + valueSupTotalSupply.toString());

        const totalS = await fifiCO.getTotalSupply();

        console.log("le 2eme : " + totalS.toString());

        console.log("le 3eme :" + (await fifiCO.calculateTokensToBuy(valueSupTotalSupply)).toString());
        // await expectRevert.unspecified(fifiCO.buyTokens({ from: buyer1, value: ether(valueSupTotalSupply) }));
    });

    // it("Achat de tokens", async () => {
    //     expect(await fifiCO.buyTokens.call({ from: buyer1, value: new BN(2000) })).to.be.true;
    //     expectEvent(await fifiCO.buyTokens({ from: buyer1, value: new BN(2000) }), "Sell", {
    //         _buyer: buyer1,
    //         _amount: new BN(2000)
    //     });
    // });

    // it("Achat de 0 token", async () => {
    //     await expectRevert(fifiCO.buyTokens({
    //         from: buyer1,
    //         value: 0
    //     }), "Toi donner sous sivouplé, sinon toi pas FiFiToken");
    // });
    //
    // it("Achat supérieur au total des tokens", async () => {
    //     // const montant = new BN((rate.mul(initialAmount)).add(new BN(1)));
    //     const montant = initialAmount.add(new BN(10));
    //     // console.log(montant);
    //     // console.log(BN(await token.totalSupply()).toNumber());
    //     await expectRevert.unspecified(fifiCO.buyTokens({ from: buyer1, value: ether(montant) }));
    //     //expect(await fifiCO.buyTokens.call({ from: buyer1, value: montant })).to.be.true;
    // });
    //
    // it("Achat égal au total des token", async () => {
    //     expect(await fifiCO.buyTokens.call({ from: buyer1, value: initialAmount })).to.be.true;
    // });
    //
    // it("Achat supérieur aux tokens déjà vendus additionnés de la quantité souhaitée", async () => {
    //     const achatInitial = new BN(60);
    //     const achatDebordant = new BN(31);
    //     await fifiCO.buyTokens({ from: buyer1, value: achatInitial });
    //     console.log(BN(await fifiCO.getTokenSold()).toNumber());
    //     //await expectRevert(fifiCO.buyTokens({ from: buyer1, value: achatDebordant}),"coucou");
    // });

});
