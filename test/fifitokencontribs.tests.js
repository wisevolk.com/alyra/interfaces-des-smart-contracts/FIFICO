const { BN, expectRevert } = require("@openzeppelin/test-helpers");
const { expect } = require("chai");

const FiFiTokenContribs = artifacts.require("FiFiTokenContribs");

contract("FiFiTokenContribs", accounts => {
    const minContrib = new BN(1000);
    const maxContrib = new BN(10000);

    let fiFiTokenContribs;

    beforeEach(async ()=> {
       fiFiTokenContribs = await FiFiTokenContribs.new();
    });

    it("Set minContrib value with maxContrib set to 0 - Init time", async () => {
        await fiFiTokenContribs.setMinContrib(minContrib);
        expect(await fiFiTokenContribs.getMinContrib()).to.be.bignumber.equal(minContrib);
        expect(await fiFiTokenContribs.getMaxContrib()).to.be.bignumber.equal(minContrib);
    });

    it("Set minContrib value with maxContrib set < maxContrib", async () => {
        const badMaxContrib = new BN(100);
        await fiFiTokenContribs.setMaxContrib(badMaxContrib);
        await expectRevert.unspecified(fiFiTokenContribs.setMinContrib(minContrib));
    });

    it("Set minContrib and maxContrib with good values", async () => {
        await fiFiTokenContribs.setMinContrib(minContrib);
        await fiFiTokenContribs.setMaxContrib(maxContrib);
        expect(await fiFiTokenContribs.getMinContrib()).to.be.bignumber.equal(minContrib);
        expect(await fiFiTokenContribs.getMaxContrib()).to.be.bignumber.equal(maxContrib);
    });

    it("Set maxContrib with maxContrib < minContrib", async () => {
        const badMaxContrib = new BN(100);
        await fiFiTokenContribs.setMinContrib(minContrib);
        await expectRevert.unspecified(fiFiTokenContribs.setMaxContrib(badMaxContrib));
    });

});
