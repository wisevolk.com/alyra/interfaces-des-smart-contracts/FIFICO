const { BN, expectRevert } = require("@openzeppelin/test-helpers");
const { expect } = require("chai");

const FiFiTokenCaps = artifacts.require("FiFiTokenCaps");

contract("FiFiTokenCaps", async accounts => {
    const softCap = new BN(1000);
    const hardCap = new BN(100000000);

    let fiFiTokenCaps;

    beforeEach(async () => {
        fiFiTokenCaps = await FiFiTokenCaps.new();
    });

    it("Set softCap value with harCap set to 0 - Init time", async () => {
        await fiFiTokenCaps.setSoftCap(softCap);
        expect(await fiFiTokenCaps.getSoftCap()).to.be.bignumber.equal(softCap);
        expect(await fiFiTokenCaps.getHardCap()).to.be.bignumber.equal(softCap);

    });

    it("Set softCap value with harCap set < softCap", async () => {
        const badHardCap = new BN(100);
        await fiFiTokenCaps.setHardCap(badHardCap);
        await expectRevert.unspecified(fiFiTokenCaps.setSoftCap(softCap));
    });

    it("Set softCap and harCap with good values", async () => {
        await fiFiTokenCaps.setSoftCap(softCap);
        await fiFiTokenCaps.setHardCap(hardCap);
        expect(await fiFiTokenCaps.getSoftCap()).to.be.bignumber.equal(softCap);
        expect(await fiFiTokenCaps.getHardCap()).to.be.bignumber.equal(hardCap);
    });

    it("Set hardCap with hardCap < softCap", async () => {
        const badHardCap = new BN(100);
        await fiFiTokenCaps.setSoftCap(softCap);
        await expectRevert.unspecified(fiFiTokenCaps.setHardCap(badHardCap));
    });

});
